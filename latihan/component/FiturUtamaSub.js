import React from 'react';
import { TouchableOpacity, Image, Text, View, Dimensions, StyleSheet, StatusBar} from 'react-native';


const FiturutamaSub = (props) => {
  
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.container}>
        <Image style={styles.imageFiturUtama} source={props.image} />
        <Text style={styles.textFitur}>{props.title}</Text>
        
    </TouchableOpacity>
  );
};

export default FiturutamaSub;

const styles = StyleSheet.create({
  container: {
    width:'25%',
    alignItems:'center',
    // backgroundColor:'red'
  },
  imageFiturUtama: {
    height:40,
    width:40,
    marginTop:10
  },
  textFitur: {
    textAlign:'center',
    marginTop:4,
  },

});