import React from 'react';
import { StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import PromoItemsSub from './PromoItemsSub';


const {height,width} = Dimensions.get('window')
const PromoItems = () => {
  return (
    <View style={styles.container}>
      <PromoItemsSub
        image={{ uri : "https://th.bing.com/th/id/OIP.5hDEmhJcgnGbqTGg52LQLwHaFj?w=255&h=191&c=7&r=0&o=5&pid=1.7" }}
        text="Sate"
        diskon="Diskon 40%"
        masaBerlaku="Until 31 juli 2022"
      />

      <PromoItemsSub
        image={{ uri : "https://th.bing.com/th/id/OIP.rxv7X73wYOvtKkNikJg1iAHaFj?w=214&h=180&c=7&r=0&o=5&pid=1.7" }}
        text="Ayam Betutu"
        diskon="Diskon 30%"
        masaBerlaku="Until 31 juli 2022"
      />

      <PromoItemsSub
        image={{ uri : "https://th.bing.com/th/id/OIP.C7nIKDppFGkCAdztaQOdUAHaE8?w=220&h=180&c=7&r=0&o=5&pid=1.7" }}
        text="Nasi Goreng"
        diskon="Diskon 20%"
        masaBerlaku="Until 31 juli 2022"
      />

      <PromoItemsSub
        image={{ uri : "https://th.bing.com/th/id/OIP.DbalH6PwUaf5BU8EpB5EygHaF0?w=196&h=180&c=7&r=0&o=5&pid=1.7" }}
        text="Ayam geppek"
        diskon="Diskon 24%"
        masaBerlaku="Until 31 juli 2022"
      />

      <PromoItemsSub
        image={{ uri : "https://lh6.googleusercontent.com/proxy/s1s40W_0gSVkKtWc-wjz1GsKuVRAmA3rDzVKNgZmOO5F70MtJFzANkjgKgtRTrW4E-svA8DUOEJd9P8wSyg7eRXFLWHKhv8f7mTr7p4sG-gSmik1-lRDbOp99PTjoTx6ypZht49W4Qs=s0-d" }}
        text="Kaldu sapi"
        diskon="Diskon 22%"
        masaBerlaku="Until 31 juli 2022"
      />

      <PromoItemsSub
        image={{ uri : "https://th.bing.com/th/id/OIP.C7nIKDppFGkCAdztaQOdUAHaE8?w=220&h=180&c=7&r=0&o=5&pid=1.7" }}
        text="Nasi Goreng"
        diskon="Diskon 12%"
        masaBerlaku="Until 31 juli 2022"
      />

      <PromoItemsSub
        image={{ uri : "https://th.bing.com/th/id/OIP.e9sUEf9xGdAF7ybb0m8aagHaD_?w=277&h=180&c=7&r=0&o=5&pid=1.7" }}
        text="Ikan Bakar"
        diskon="Diskon 30%"
        masaBerlaku="Until 31 juli 2022"
      />

      <PromoItemsSub
        image={{ uri : "https://cdn.idntimes.com/content-images/community/2017/09/1-mie-ayam-subur-aaad9a236e93bcf0dafb0470ebb366be.jpg" }}
        text="Mie Ayam"
        diskon="Diskon 10%"
        masaBerlaku="Until 31 juli 2022"
      />
  
  
    </View>
    
  );
};

export default PromoItems;

const styles = StyleSheet.create({
  container: {
    paddingTop:15,
    // marginHorizontal:18,
    paddingLeft:18,
    width:'100%',
    flexWrap:'wrap',
    flexDirection:'row',
    backgroundColor:'#fff'
  }
});